# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <registryFunction.h>
# include <epicsExport.h>
# include <aSubRecord.h>
# include <time.h>

static int sources(aSubRecord *precord) 
{
    FILE *fp;
    char cmd_output[1000];
    char data[20][100];
    int  stringlen[20];
    char *token;
    int i = 0;
    int j = 0;
    char empty_line = '1';
    char active_server;
    char normal_output[20] = "sources";
    time_t now;
    char chrony_cmd[50] = "chronyc sources";
    int status_bool = 0;
    char error_string[1000];

    // Get time
    time(&now);

    // Open the command for reading.
    fp = popen(chrony_cmd, "r");
    if (fp == NULL) 
    {
        printf("Failed to run command\n" );
        pclose(fp);
        return 0;
    }

    memset(cmd_output, '\0', sizeof(cmd_output));
    if (fgets(cmd_output, sizeof(cmd_output)-1, fp) == NULL)
    {
        empty_line = '\0';
    }
    
    //If normal output is found, continue, otherwise, return 
    if (strstr(cmd_output,normal_output) == NULL)
    {
       // printf("%s\n%s%s\n", chrony_cmd, ctime(&now), cmd_output);
        strcat(error_string, chrony_cmd);
        strcat(error_string, ctime(&now));
        strcat(error_string, cmd_output);
        strcpy(precord->valh, error_string);
        *(long *)precord->valg = status_bool;
        pclose(fp);
        return 0;
    }

    // Read the output a line at a time - output it.
    while (empty_line != '\0') 
    {
        token = strtok(cmd_output," []");
        if (strlen(cmd_output) > 0) 
        {
            active_server = cmd_output[1];
        } else
        {
            active_server = '\0';
        }
        //Parse row and look for connected server with '*'
        while(token != NULL && active_server == '*')
        {
            memset(data[i], '\0', sizeof(data[i]));
            stringlen[i] = strlen(token);
            for (j = 0; j < stringlen[i]; j++)
            {
                data[i][j] = token[j];
            }

            i++;
            token = strtok(NULL, " []");
        }

        if (active_server == '*')
        {
            //Format data
            strcat(data[6],data[7]);
            strcat(data[6],"[");
            strcat(data[6],data[8]);
            strcat(data[6],"]");
            strcat(data[6],data[9]);
            strcat(data[6],data[10]);
            //printf("after %s, strlen %d\n", data[6], (int) strlen(data[6]));
        }
        //get the next output line
        memset(cmd_output, '\0', sizeof(cmd_output));
        if (fgets(cmd_output, sizeof(cmd_output)-1, fp) == NULL)
        {
           empty_line = '\0';
        }
    }

    pclose(fp);
    status_bool = 1;
    //name_ip
    strcpy(precord->vala, data[1]);
    //stratum level
    *(long *)precord->valb = atoi(data[2]);
    //poll rate
    *(long *)precord->valc = atoi(data[3]);
    //reach
    *(long *)precord->vald = atoi(data[4]);
    //last received
    *(long *)precord->vale = atoi(data[5]);
    //last sample
    strcpy(precord->valf, data[6]);
    //status bool
    *(long *)precord->valg = status_bool;

    return 0;
}

epicsRegisterFunction(sources);
