# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <registryFunction.h>
# include <epicsExport.h>
# include <aSubRecord.h>
# include <time.h>

static int tracking(aSubRecord *precord) 
{
    FILE *fp;
    char cmd_output[1000];
    char data[20][100];
    char *token;
    int i = 0;
    char empty_line = '1';
    char trimmed_output[1000];
    char normal_output[20] = "Reference ID";
    time_t now;
    char chrony_cmd[50] = "chronyc tracking";
    int status_bool = 0;
    char error_string[1000];

    //Get time
    time(&now);

    // Open the command for reading.
    fp = popen(chrony_cmd, "r");
    if (fp == NULL) 
    {
        printf("Failed to run command\n" );
        pclose(fp);
        return 0;
    }

    memset(cmd_output, '\0', sizeof(cmd_output));

    // Read first line of output
    if (fgets(cmd_output, sizeof(cmd_output)-1, fp) == NULL)
    {
        empty_line = '\0';
    }
   
    //If normal output is found, continue, otherwise, return 
    if (strncmp(cmd_output,normal_output, strlen(normal_output)) != 0)
    {
//        printf("%s\n%s%s\n",chrony_cmd, ctime(&now), cmd_output);
        strcat(error_string, chrony_cmd);
        strcat(error_string, ctime(&now));
        strcat(error_string, cmd_output); 
        strcpy(precord->valo, error_string);
        *(long *)precord->valn = status_bool;
        pclose(fp);
        return 0;
    }
    // Read the output a line at a time - output it.
    while (empty_line != '\0')
    {
        memset(trimmed_output, '\0', sizeof(trimmed_output));
        token = strpbrk(cmd_output,":");
        token = strpbrk(token," ");
        strncpy(trimmed_output,token,strlen(token));
        memset(data[i], '\0', sizeof(data[i]));

        //Use the full string for some output rows
        if (i != 0 && i != 2 && i != 12 && token != NULL)
        {
            token = strtok(trimmed_output," ");
            token = strtok(NULL," ");
        }
        token = strtok(trimmed_output,"\n");
        strncpy(data[i],token,strlen(token));
        i++;
        
        //get the next output line
        memset(cmd_output, '\0', sizeof(cmd_output));
        if (fgets(cmd_output, sizeof(cmd_output)-1, fp) == NULL)
        {
            empty_line = '\0';
        }
    }

    pclose(fp);
    status_bool = 1; 
    //reference id
    strcpy(precord->vala, data[0]);
    //stratum
    *(long *)precord->valb = atoi(data[1]);
    //reference time (UTC)
    strcpy(precord->vala, data[2]);
    //system time
    *(double *)precord->vald = atof(data[3]);
    //last offset
    *(double *)precord->vale = atof(data[4]);
    //RMS offset
    *(double *)precord->valf = atof(data[5]);
    //frequency
    *(double *)precord->valg = atof(data[6]);
    //residual frequency
    *(double *)precord->valh = atof(data[7]);
    //skew
    *(double *)precord->vali = atof(data[8]);
    //root delay
    *(double *)precord->valj = atof(data[9]);
    //root dispersion
    *(double *)precord->valk = atof(data[10]);
    //update interval
    *(double *)precord->vall = atof(data[11]);
    //leap status
    strcpy(precord->valm, data[12]);
    //status boolean, true = ok
    *(long *)precord->valn = status_bool;
    return 0;
}

epicsRegisterFunction(tracking);
